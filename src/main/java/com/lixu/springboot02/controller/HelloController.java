package com.lixu.springboot02.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 控制器
 * @author Administrator
 *
 */
@Controller
public class HelloController {
	@RequestMapping("/hello")
	@ResponseBody
	public Map<String,Object> showHello(){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("say:", "helloworld");
		return map;
	}
}
